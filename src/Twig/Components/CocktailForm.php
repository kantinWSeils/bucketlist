<?php

namespace App\Twig\Components;

use App\Form\CocktailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class CocktailForm extends AbstractController
{
	use ComponentWithFormTrait;
    use DefaultActionTrait;

	protected function instantiateForm(): FormInterface
	{
		return $this->createForm(CocktailType::class);
	}

}
