<?php

namespace App\Factory;

use App\Entity\Color;
use App\Repository\ColorRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Color>
 *
 * @method        Color|Proxy                     create(array|callable $attributes = [])
 * @method static Color|Proxy                     createOne(array $attributes = [])
 * @method static Color|Proxy                     find(object|array|mixed $criteria)
 * @method static Color|Proxy                     findOrCreate(array $attributes)
 * @method static Color|Proxy                     first(string $sortedField = 'id')
 * @method static Color|Proxy                     last(string $sortedField = 'id')
 * @method static Color|Proxy                     random(array $attributes = [])
 * @method static Color|Proxy                     randomOrCreate(array $attributes = [])
 * @method static ColorRepository|RepositoryProxy repository()
 * @method static Color[]|Proxy[]                 all()
 * @method static Color[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Color[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Color[]|Proxy[]                 findBy(array $attributes)
 * @method static Color[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Color[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class ColorFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->colorName(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Color $color): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Color::class;
    }
}
