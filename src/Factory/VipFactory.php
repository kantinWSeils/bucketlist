<?php

namespace App\Factory;

use App\Entity\Vip;
use App\Repository\VipRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Vip>
 *
 * @method        Vip|Proxy                     create(array|callable $attributes = [])
 * @method static Vip|Proxy                     createOne(array $attributes = [])
 * @method static Vip|Proxy                     find(object|array|mixed $criteria)
 * @method static Vip|Proxy                     findOrCreate(array $attributes)
 * @method static Vip|Proxy                     first(string $sortedField = 'id')
 * @method static Vip|Proxy                     last(string $sortedField = 'id')
 * @method static Vip|Proxy                     random(array $attributes = [])
 * @method static Vip|Proxy                     randomOrCreate(array $attributes = [])
 * @method static VipRepository|RepositoryProxy repository()
 * @method static Vip[]|Proxy[]                 all()
 * @method static Vip[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Vip[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Vip[]|Proxy[]                 findBy(array $attributes)
 * @method static Vip[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Vip[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class VipFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'firstName' => self::faker()->firstName(),
            'lastName' => self::faker()->lastName(),
            'isAvailable' => self::faker()->boolean(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Vip $vip): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Vip::class;
    }
}
