<?php

namespace App\Factory;

use App\Entity\Fruit;
use App\Repository\FruitRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Fruit>
 *
 * @method        Fruit|Proxy                     create(array|callable $attributes = [])
 * @method static Fruit|Proxy                     createOne(array $attributes = [])
 * @method static Fruit|Proxy                     find(object|array|mixed $criteria)
 * @method static Fruit|Proxy                     findOrCreate(array $attributes)
 * @method static Fruit|Proxy                     first(string $sortedField = 'id')
 * @method static Fruit|Proxy                     last(string $sortedField = 'id')
 * @method static Fruit|Proxy                     random(array $attributes = [])
 * @method static Fruit|Proxy                     randomOrCreate(array $attributes = [])
 * @method static FruitRepository|RepositoryProxy repository()
 * @method static Fruit[]|Proxy[]                 all()
 * @method static Fruit[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Fruit[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Fruit[]|Proxy[]                 findBy(array $attributes)
 * @method static Fruit[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Fruit[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class FruitFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->words(1, true),
			'color' => ColorFactory::new(),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this
            // ->afterInstantiate(function(Fruit $fruit): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Fruit::class;
    }
}
