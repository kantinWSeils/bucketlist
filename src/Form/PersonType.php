<?php

namespace App\Form;

use App\Entity\Person;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
				'attr' => [
					'placeholder' => 'First name'
				]
			])
            ->add('name', TextType::class, [
				'attr' => [
					'placeholder' => 'Last name'
				]
			])
            ->add('teams', EntityType::class, [
                'class' => Team::class,
				'choice_label' => 'name',
				'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
