<?php

namespace App\Form;

use App\Entity\Cocktail;
use App\Entity\Color;
use App\Entity\Fruit;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfonycasts\DynamicForms\DependentField;
use Symfonycasts\DynamicForms\DynamicFormBuilder;

class CocktailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
		$builder = new DynamicFormBuilder($builder);

        $builder
            ->add('name')
			->add('color', EntityType::class, [
				'class' => Color::class,
				'mapped' => false,
				'choice_label' => 'name',
			])
			->addDependent('fruit', 'color', function(DependentField $field, ?Color $color) {
				$field->add(EntityType::class, [
					'class' => Fruit::class,
					'choices' => $color?->getFruits(),
					'choice_label' => 'name',
					'disabled' => null === $color,
				]);
			})
//            ->add('fruit', EntityType::class, [
//                'class' => Fruit::class,
////				'mapped' => false,
//				'choice_label' => 'name',
//            ])
			->add('save', SubmitType::class, [
				'label' => 'Save',
			])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cocktail::class,
        ]);
    }
}
