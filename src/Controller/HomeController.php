<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
	public function __construct(private readonly CategoryRepository $categoryRepository)
	{
	}

	#[Route('/', name: 'app_home')]
	public function index(): Response
	{
		$categories = $this->categoryRepository->findAllPublishedWishesByCategory();

		return $this->render('home/index.html.twig', [
			'categories' => $categories,
		]);
	}

	#[Route('/about-us', name: 'app_about')]
	public function aboutUs(): Response
	{
		return $this->render('home/about-us.html.twig', []);
	}

	#[Route('/vip', name: 'app_vip')]
	public function vipCocktail(): Response
	{
		return $this->render('vip/index.html.twig', []);
	}
}
