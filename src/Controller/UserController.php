<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditUserType;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/user', name: 'app_user')]
class UserController extends AbstractController
{
	public function __construct(private readonly UserRepository $userRepository)
	{
	}

	#[Route('/list', name: '_list')]
    public function index(): Response
    {
		$users = $this->userRepository->findAll();

        return $this->render('user/list.html.twig', [
            'users' => $users,
        ]);
    }

	#[Route('/new', name: '_new')]
	public function new(Request $request,UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
	{
		$user = new User();

		// Create the wish form
		$userForm = $this->createForm(RegistrationFormType::class, $user);
		$userForm->handleRequest($request);

		// If the form is submitted and valid
		if ($userForm->isSubmitted() && $userForm->isValid()) {
			// encode the plain password
			$user->setPassword(
				$userPasswordHasher->hashPassword(
					$user,
					$userForm->get('plainPassword')->getData()
				)
			);
			$user->setRoles(['ROLE_USER']);

			// Persist the new wish to the database
			$entityManager->persist($user);
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'New user successfully added !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_user_list');
		}

		return $this->render('user/new.html.twig', [
			'form' => $userForm,
		]);
	}

	#[Route('/edit/{id}', name: '_edit', requirements: ['page' => '\d+'])]
	public function edit(User $user, Request $request, EntityManagerInterface $entityManager): Response
	{
		// Create the user form
		$userForm = $this->createForm(EditUserType::class, $user);
		$userForm->handleRequest($request);

		// If the form is submitted and valid
		if ($userForm->isSubmitted() && $userForm->isValid()) {
			// Persist the edited wish to the database
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'User successfully edited !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_user_list');
		}

		return $this->render('user/edit.html.twig', [
			'user' => $user,
			'form' => $userForm,
		]);
	}

	#[Route('/delete/{id}', name: '_delete', requirements: ['page' => '\d+'])]
	public function delete(User $user, EntityManagerInterface $entityManager): Response
	{
		$entityManager->remove($user);
		$entityManager->flush();

		// Add a success flash message
		$this->addFlash('success', 'User successfully deleted !');

		// Redirect to the wish list page
		return $this->redirectToRoute('app_user_list');
	}
}
