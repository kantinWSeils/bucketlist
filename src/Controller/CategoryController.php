<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Form\EditCategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/category', name: 'app_category')]
class CategoryController extends AbstractController
{
	public function __construct(private readonly CategoryRepository $categoryRepository)
	{
	}

	#[Route('/list', name: '_list')]
	public function index(): Response
	{
		$categories = $this->categoryRepository->findAll();

		return $this->render('category/list.html.twig', [
			'categories' => $categories,
		]);
	}

	#[Route('/new', name: '_new')]
	public function new(Request $request, EntityManagerInterface $entityManager): Response
	{
		$category = new Category();

		// Create the category form
		$categoryForm = $this->createForm(CategoryType::class, $category);
		$categoryForm->handleRequest($request);

		// If the form is submitted and valid
		if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
			// Persist the new category to the database
			$entityManager->persist($category);
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'New category successfully added !');

			// Redirect to the category list page
			return $this->redirectToRoute('app_category_list');
		}

		return $this->render('category/new.html.twig', [
			'form' => $categoryForm,
		]);
	}

	#[Route('/edit/{id}', name: '_edit', requirements: ['page' => '\d+'])]
	public function edit(Category $category, Request $request, EntityManagerInterface $entityManager): Response
	{
		// Create the category form
		$categoryForm = $this->createForm(EditCategoryType::class, $category);
		$categoryForm->handleRequest($request);

		// If the form is submitted and valid
		if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
			// Persist the edited wish to the database
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'Category successfully edited !');

			// Redirect to the category list page
			return $this->redirectToRoute('app_category_list');
		}

		return $this->render('category/edit.html.twig', [
			'category' => $category,
			'form' => $categoryForm,
		]);
	}

	#[Route('/delete/{id}', name: '_delete', requirements: ['page' => '\d+'])]
	public function delete(Category $category, EntityManagerInterface $entityManager): Response
	{
		$entityManager->remove($category);
		$entityManager->flush();

		// Add a success flash message
		$this->addFlash('success', 'Category successfully deleted !');

		// Redirect to the category list page
		return $this->redirectToRoute('app_category_list');
	}
}
