<?php

namespace App\Controller;

use App\Entity\Vip;
use App\Repository\VipRepository;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/vip', name: 'app_api_vip')]
class VipController extends AbstractController
{
	public function __construct(private readonly VipRepository $vipRepository)
	{
	}

	#[Route('', name: '_list', methods: ['GET'])]
	public function getAll(): Response
	{
		$vips = $this->vipRepository->findAll();

		return $this->json($vips);
	}

	#[Route('/{id}', name: '_item', requirements: ['page' => '\d+'], methods: ['GET'])]
	public function getById(int $id): Response
	{
		$vip = $this->vipRepository->find($id);

		return $this->json($vip);
	}

	#[Route('', name: '_add', methods: ['POST'])]
	public function add(Request $request, EntityManagerInterface $entityManager): Response
	{
		$data = $request->toArray();

		$newVip = new Vip();
		$newVip->setFirstName($data['firstName']);
		$newVip->setLastName($data['lastName']);
		$newVip->setIsAvailable($data['isAvailable']);

		$entityManager->persist($newVip);
		$entityManager->flush();

		return new Response(Response::HTTP_OK);
	}

	#[Route('/{id}', name: '_edit', requirements: ['page' => '\d+'], methods: ['PUT'])]
	public function edit(Vip $vip, EntityManagerInterface $entityManager): Response
	{
		$vip->setIsAvailable(!$vip->isIsAvailable());
		$entityManager->flush();

		return new Response(Response::HTTP_OK);
	}

	#[Route('/{id}', name: '_delete', requirements: ['page' => '\d+'], methods: ['DELETE'])]
	public function delete(Vip $vip, EntityManagerInterface $entityManager): Response
	{
		$entityManager->remove($vip);
		$entityManager->flush();

		return new Response(Response::HTTP_OK);
	}
}
