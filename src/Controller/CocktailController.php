<?php

namespace App\Controller;

use App\Entity\Cocktail;
use App\Form\CocktailType;
use App\Repository\CocktailRepository;
use App\Repository\ColorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/cocktail', name: 'app_cocktail')]
class CocktailController extends AbstractController
{
	public function __construct(private readonly CocktailRepository $cocktailRepository, private readonly ColorRepository $colorRepository)
	{
	}

	#[Route('/list', name: '_list')]
	public function index(): Response
	{
		$cocktails = $this->cocktailRepository->findAll();

		return $this->render('cocktail/list.html.twig', ['cocktails' => $cocktails,]);
	}

	#[Route('/new', name: '_new')]
	public function new(Request $request, EntityManagerInterface $entityManager): Response
	{
		$cocktail = new Cocktail();

		// Create the wish form
		$cocktailForm = $this->createForm(CocktailType::class);
		$cocktailForm->handleRequest($request);

		// If the form is submitted and valid
		if ($cocktailForm->isSubmitted() && $cocktailForm->isValid()) {
			// Persist the new wish to the database
			$entityManager->persist($cocktail);
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'New cocktail successfully added !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_cocktail_list');
		}

		return $this->render('cocktail/new.html.twig', ['form' => $cocktailForm,]);
	}

	#[Route('/new-data', name: '_new_data', methods: ['GET'])]
	public function newData(Request $request): Response
	{
		$colors = $this->colorRepository->findAll();

		return $this->json($colors);
	}
}
