<?php

namespace App\Controller;

use App\Entity\Wish;
use App\Form\EditWishType;
use App\Form\WishType;
use App\Repository\WishRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/wish', name: 'app_wish')]
class WishController extends AbstractController
{
	public function __construct(private readonly WishRepository $wishRepository)
	{
	}

	#[Route('/list', name: '_list')]
	public function list(): Response
	{
		$wishes = $this->wishRepository->findAll();

		return $this->render('wish/list.html.twig', [
			'wishes' => $wishes,
		]);
	}

	#[Route('/new', name: '_new')]
	public function new(Request $request, EntityManagerInterface $entityManager): Response
	{
		$wish = new Wish();

		// Create the wish form
		$wishForm = $this->createForm(WishType::class, $wish);
		$wishForm->handleRequest($request);

		// If the form is submitted and valid
		if ($wishForm->isSubmitted() && $wishForm->isValid()) {
			$wish->setIsPublished(true);
			$wish->setCreatedAt(new \DateTimeImmutable());

			// Persist the new wish to the database
			$entityManager->persist($wish);
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'New wish successfully !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_wish_list');
		}

		return $this->render('wish/new.html.twig', ['form' => $wishForm,]);
	}

	#[Route('/{id}', name: '_detail', requirements: ['page' => '\d+'])]
	public function detail(Wish $wish): Response
	{
		return $this->render('wish/detail.html.twig', ['wish' => $wish,]);
	}

	#[Route('/edit/{id}', name: '_edit', requirements: ['page' => '\d+'])]
	public function edit(Wish $wish, Request $request, EntityManagerInterface $entityManager): Response
	{
		// Create the wish form
		$wishForm = $this->createForm(EditWishType::class, $wish);
		$wishForm->handleRequest($request);

		// If the form is submitted and valid
		if ($wishForm->isSubmitted() && $wishForm->isValid()) {
			// Persist the edited wish to the database
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'Wish successfully edited !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_wish_list');
		}

		return $this->render('wish/edit.html.twig', ['wish' => $wish, 'form' => $wishForm,]);
	}

	#[Route('/delete/{id}', name: '_delete', requirements: ['page' => '\d+'])]
	public function delete(Wish $wish, EntityManagerInterface $entityManager): Response
	{
		$entityManager->remove($wish);
		$entityManager->flush();

		// Add a success flash message
		$this->addFlash('success', 'Wish successfully deleted !');

		// Redirect to the wish list page
		return $this->redirectToRoute('app_wish_list');
	}
}
