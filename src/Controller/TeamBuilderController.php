<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Team;
use App\Form\PersonType;
use App\Form\TeamType;
use App\Repository\PersonRepository;
use App\Repository\TeamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class TeamBuilderController extends AbstractController
{
	public function __construct(private readonly TeamRepository $teamRepository, private readonly PersonRepository $personRepository)
	{
	}

	#[Route('/team-builder', name: 'app_team_builder')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
		$team = new Team();

		$teamForm = $this->createForm(TeamType::class, $team);
		$teamForm->handleRequest($request);

		// If the form is submitted and valid
		if ($teamForm->isSubmitted() && $teamForm->isValid()) {
			// Persist the new wish to the database
			$entityManager->persist($team);
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'New team successfully added !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_team_builder');
		}

		$person = new Person();

		$personForm = $this->createForm(PersonType::class, $person);
		$personForm->handleRequest($request);

		// If the form is submitted and valid
		if ($personForm->isSubmitted() && $personForm->isValid()) {
			$person->addTeam($personForm['teams']->getData());

			// Persist the new wish to the database
			$entityManager->persist($person);
			$entityManager->flush();

			// Add a success flash message
			$this->addFlash('success', 'New person successfully added !');

			// Redirect to the wish list page
			return $this->redirectToRoute('app_team_builder');
		}

		return $this->render('team_builder/index.html.twig', [
			'teams' => $this->teamRepository->findAll(),
			'persons' => $this->personRepository->findAll(),
			'teamForm' => $teamForm,
			'personForm' => $personForm,
		]);
    }

	#[Route('/team-builder/delete-team/{id}', name: 'app_team_builder_delete_team', requirements: ['page' => '\d+'])]
	public function deleteTeam(Team $team, EntityManagerInterface $entityManager): Response
	{
		// Add a success flash message
		$this->addFlash('success', 'Team successfully deleted !');

		$entityManager->remove($team);
		$entityManager->flush();

		// Redirect to the wish list page
		return $this->redirectToRoute('app_team_builder');
	}

	#[Route('/team-builder/delete-person/{id}', name: 'app_team_builder_delete_person', requirements: ['page' => '\d+'])]
	public function deletePerson(Person $person, EntityManagerInterface $entityManager): Response
	{
		// Add a success flash message
		$this->addFlash('success', 'Person successfully deleted !');

		$entityManager->remove($person);
		$entityManager->flush();

		// Redirect to the wish list page
		return $this->redirectToRoute('app_team_builder');
	}
}
