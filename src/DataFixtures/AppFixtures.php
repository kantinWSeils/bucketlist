<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\CategoryFactory;
use App\Factory\CocktailFactory;
use App\Factory\ColorFactory;
use App\Factory\FruitFactory;
use App\Factory\UserFactory;
use App\Factory\VipFactory;
use App\Factory\WishFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
	public function __construct(private UserPasswordHasherInterface $passwordHasher)
	{
	}

	public function load(ObjectManager $manager): void
	{
		// Create an admin user
		$adminUser = new User();
		$adminUser->setEmail('kvisset@w-seils.com');
		$adminUser->setUsername('Kantin');
		$adminUser->setPassword($this->passwordHasher->hashPassword($adminUser, 'admin'));
		$adminUser->setRoles(['ROLE_ADMIN']);

		$manager->persist($adminUser);

		// Wish fixtures
		CategoryFactory::createMany(6);

		WishFactory::createMany(25, function() {
			return ['category' => CategoryFactory::random()];
		});

		// User fixtures
		UserFactory::createMany(5);

		// Cocktail fixtures
		ColorFactory::createMany(5);

		FruitFactory::createMany(30, function() {
			return ['color' => ColorFactory::random()];
		});

		CocktailFactory::createMany(10, function() {
			return ['fruit' => FruitFactory::random()];
		});

		// Vip fixtures
		VipFactory::createMany(10);

		$manager->flush();
	}
}
